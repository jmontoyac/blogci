#!/bin/bash

FILE=./database.db
if [ -f "$FILE" ]; then
    echo "$FILE exists."
else 
    echo "$FILE does not exist."
    python init_db.py
fi