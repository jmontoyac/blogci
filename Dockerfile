# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

ENV VIRTUAL_ENV=venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
ENV FLASK_APP=app
ENV FLASK_ENV=development

RUN apt update && apt install curl -y

# Install dependencies:
COPY requirements.txt .
RUN pip3 install --upgrade pip && pip3 install -r requirements.txt && pip3 install --no-cache-dir Flask \
    flask_prometheus_metrics prometheus_client

# Run the application:
COPY . .
#RUN python init_db.py
EXPOSE 5000
#CMD [ "flask", "run" ]
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]